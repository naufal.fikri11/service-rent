package b13.rentspace.servicerent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceRentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceRentApplication.class, args);
	}

}
