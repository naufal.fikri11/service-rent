package b13.rentspace.servicepayment.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Generated;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Order(1)
@Generated
public class SecurityFilter implements Filter {

    //    @Value("${API_KEY}")
    private final String apiKey = "7687746c-5b25-42cc-b7f2-b86b89e8c8c2";

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String token = req.getHeader("X-API-Key");

        if (!token.equals(apiKey)){
            resp.sendError(403, "You don't have access");
        }
        else{
            chain.doFilter(request, response);
        }
    }
}
