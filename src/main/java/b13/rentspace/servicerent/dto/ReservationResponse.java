package b13.rentspace.servicerent.dto;

import b13.rentspace.servicerent.model.reservation.Reservation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReservationResponse {
    private Integer id;
    private Integer customerID;
    private Integer spaceID;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private Boolean isValid;

    public static ReservationResponse fromReservation(Reservation reservation){
        return ReservationResponse.builder()
                .id(reservation.getId())
                .customerID(reservation.getCustomerID())
                .spaceID(reservation.getSpaceID())
                .dateStart(reservation.getDateStart())
                .dateEnd(reservation.getDateEnd())
                .isValid(reservation.getIsValid())
                .build();
    }
}
