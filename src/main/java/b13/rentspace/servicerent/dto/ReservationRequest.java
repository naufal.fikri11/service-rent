package b13.rentspace.servicerent.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReservationRequest {
    private Integer Id;
    private Integer customerID;
    private Integer spaceID;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private Boolean isValid;
}