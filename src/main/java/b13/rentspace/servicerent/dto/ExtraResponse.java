package b13.rentspace.servicerent.dto;

import b13.rentspace.servicerent.model.extra_service.Extra;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExtraResponse {
    private Integer id;
    private Integer reservationID;
    private String name;

    public static ExtraResponse fromExtra(Extra extra){
        return ExtraResponse.builder()
                .id(extra.getId())
                .reservationID(extra.getReservationID())
                .name(extra.getName())
                .build();
    }
}
