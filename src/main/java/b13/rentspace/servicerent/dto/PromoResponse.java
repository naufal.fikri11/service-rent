package b13.rentspace.servicerent.dto;

import b13.rentspace.servicerent.model.promo_code.PromoCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PromoResponse {
    private Integer Id;
    private String name;
    private Integer discount;

    public static PromoResponse fromPromo(PromoCode promo){
        return PromoResponse.builder()
                .Id(promo.getId())
                .name(promo.getName())
                .discount(promo.getDiscount())
                .build();
    }
}
