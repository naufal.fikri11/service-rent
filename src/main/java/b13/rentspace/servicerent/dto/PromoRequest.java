package b13.rentspace.servicerent.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PromoRequest {
    private Integer Id;
    private String name;
    private Integer discount;
}
