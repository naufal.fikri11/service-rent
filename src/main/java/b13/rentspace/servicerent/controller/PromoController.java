package b13.rentspace.servicerent.controller;

import b13.rentspace.servicerent.dto.CreatePromoRequest;
import b13.rentspace.servicerent.dto.PromoRequest;
import b13.rentspace.servicerent.dto.PromoResponse;
import b13.rentspace.servicerent.model.promo_code.PromoCode;
import b13.rentspace.servicerent.service.promo.PromoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/rent/promo")
@RequiredArgsConstructor
public class PromoController {
    private final PromoService promoService;
    @PostMapping("/create")
    public ResponseEntity<PromoCode> createPromo(@RequestBody CreatePromoRequest createPromoRequest){
        var response = promoService.createPromoCode(createPromoRequest);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update")
    public ResponseEntity<PromoCode> updatePromo(@RequestBody PromoRequest promoRequest){
        var response = promoService.updatePromoCode(promoRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/findid/{id}")
    public ResponseEntity<PromoResponse> findPromoById(@PathVariable Integer id){
        var response = promoService.findPromoById(id);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/deleteall")
    public ResponseEntity<String> deleteAll(){
        promoService.deleteAll();
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Integer id){
        promoService.deleteById(id);
        return ResponseEntity.ok("Success");
    }
}
