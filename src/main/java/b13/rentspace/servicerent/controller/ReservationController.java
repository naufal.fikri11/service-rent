package b13.rentspace.servicerent.controller;

import b13.rentspace.servicerent.dto.CreateReservationRequest;
import b13.rentspace.servicerent.dto.ReservationRequest;
import b13.rentspace.servicerent.dto.ReservationResponse;
import b13.rentspace.servicerent.model.reservation.Reservation;
import b13.rentspace.servicerent.service.reservation.ReservationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/rent/reservation")
@RequiredArgsConstructor
public class ReservationController {
    private final ReservationService reservationService;

    @GetMapping("/test")
    public ResponseEntity<String> test(){
        return ResponseEntity.ok("Success");
    }
    @PostMapping("/create")
    public ResponseEntity<Reservation> createReservation(@RequestBody CreateReservationRequest reservationRequest){
        var response = reservationService.createReservation(reservationRequest);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update")
    public ResponseEntity<Reservation> updateReservation(@RequestBody ReservationRequest reservationRequest){
        var response = reservationService.updateReservation(reservationRequest);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/validate/{id}")
    public ResponseEntity<Reservation> validateReservation(@PathVariable Integer id){
        var response = reservationService.validateReservation(id);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<List<ReservationResponse>> getCustomerHistory(@PathVariable Integer id){
        try {
            var response = reservationService.getCustomerHistory(id);
            return ResponseEntity.ok(response);
        } catch (Exception e){
            List<ReservationResponse> response = new ArrayList<>();
            return ResponseEntity.ok(response);
        }
    }

    @GetMapping("/provider/{id}")
    public ResponseEntity<List<ReservationResponse>> getProviderHistory(@PathVariable Integer id){
        try {
            var response = reservationService.getProviderHistory(id);
            return ResponseEntity.ok(response);
        } catch (Exception e){
            List<ReservationResponse> response = new ArrayList<>();
            return ResponseEntity.ok(response);
        }
    }

    @DeleteMapping("/deleteall")
    public ResponseEntity<String> deleteAll(){
        reservationService.deleteAll();
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Integer id){
        reservationService.deleteById(id);
        return ResponseEntity.ok("Success");
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<ReservationResponse>> findAll(){
        var response = reservationService.getAllReservation();
        return ResponseEntity.ok(response);
    }
}
