package b13.rentspace.servicerent.controller;

import b13.rentspace.servicerent.dto.CreatePromoRequest;
import b13.rentspace.servicerent.dto.ExtraRequest;
import b13.rentspace.servicerent.dto.ExtraResponse;
import b13.rentspace.servicerent.model.extra_service.Extra;
import b13.rentspace.servicerent.model.promo_code.PromoCode;
import b13.rentspace.servicerent.service.extra.ExtraService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/rent/extra")
@RequiredArgsConstructor
public class ExtraController {
    private final ExtraService extraService;

    @PostMapping("/create")
    public ResponseEntity<Extra> createExtra(@RequestBody ExtraRequest extraRequest){
        var response = extraService.createExtra(extraRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/findid/{id}")
    public ResponseEntity<ExtraResponse> findExtraById(@PathVariable Integer id){
        var response = extraService.findExtraById(id);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/deleteall")
    public ResponseEntity<String> deleteAll(){
        extraService.deleteAll();
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Integer id){
        extraService.deleteById(id);
        return ResponseEntity.ok("Success");
    }
}
