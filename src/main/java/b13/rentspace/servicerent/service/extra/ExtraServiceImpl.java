package b13.rentspace.servicerent.service.extra;

import b13.rentspace.servicerent.dto.ExtraRequest;
import b13.rentspace.servicerent.dto.ExtraResponse;
import b13.rentspace.servicerent.exceptions.ExtraIDDoesntExistException;
import b13.rentspace.servicerent.model.extra_service.Extra;
import b13.rentspace.servicerent.repository.ExtraRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ExtraServiceImpl implements ExtraService {
    private final ExtraRepository ExtraRepository;

    public Extra createExtra(ExtraRequest extraRequest) {
        Extra extra = Extra.builder()
                .reservationID(extraRequest.getReservationID())
                .name(extraRequest.getName())
                .build();
        return ExtraRepository.save(extra);
    }

    public ExtraResponse findExtraById(Integer id) {
        Optional<Extra> search = ExtraRepository.findById(id);

        if (search.isEmpty()) {
            throw new ExtraIDDoesntExistException(id);
        }
        return ExtraResponse.fromExtra(search.get());
    }

    public void deleteById(Integer id) {
        Optional<Extra> search = ExtraRepository.findById(id);
        if (search.isEmpty()) {
            throw new ExtraIDDoesntExistException(id);
        }
        ExtraRepository.deleteById(id);
    }

    public void deleteAll() {
        ExtraRepository.deleteAll();
    }

}
