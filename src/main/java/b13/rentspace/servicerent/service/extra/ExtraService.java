package b13.rentspace.servicerent.service.extra;

import b13.rentspace.servicerent.dto.ExtraRequest;
import b13.rentspace.servicerent.dto.ExtraResponse;
import b13.rentspace.servicerent.model.extra_service.Extra;

public interface ExtraService {
    Extra createExtra(ExtraRequest extraRequest);
    ExtraResponse findExtraById(Integer id);
    void deleteAll();
    void deleteById(Integer id);
}
