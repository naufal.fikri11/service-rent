package b13.rentspace.servicerent.service.promo;

import b13.rentspace.servicerent.dto.CreatePromoRequest;
import b13.rentspace.servicerent.dto.PromoRequest;
import b13.rentspace.servicerent.dto.PromoResponse;
import b13.rentspace.servicerent.model.promo_code.PromoCode;
import org.springframework.stereotype.Service;

@Service
public interface PromoService {
    PromoCode createPromoCode(CreatePromoRequest createPromoRequest);
    PromoCode updatePromoCode(PromoRequest promoRequest);
    PromoCode findPromoByName(String promoName);
    PromoResponse findPromoById(Integer Id);
    void deleteAll();

    void deleteById(Integer id);
}
