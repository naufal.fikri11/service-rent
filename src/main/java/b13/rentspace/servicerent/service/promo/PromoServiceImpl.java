package b13.rentspace.servicerent.service.promo;

import b13.rentspace.servicerent.dto.CreatePromoRequest;
import b13.rentspace.servicerent.dto.PromoRequest;
import b13.rentspace.servicerent.dto.PromoResponse;
import b13.rentspace.servicerent.exceptions.PromoAlreadyExistException;
import b13.rentspace.servicerent.exceptions.PromoIDDoesntExistExeption;
import b13.rentspace.servicerent.exceptions.PromoNameDoesntExistException;
import b13.rentspace.servicerent.model.promo_code.PromoCode;
import b13.rentspace.servicerent.repository.PromoCodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PromoServiceImpl implements PromoService{
    private final PromoCodeRepository promoRepository;

    public PromoCode createPromoCode(CreatePromoRequest createPromoRequest) {
        if (findPromoByName(createPromoRequest.getName()) != null) {
            throw new PromoAlreadyExistException(createPromoRequest.getName());
        }
        PromoCode promoCode = PromoCode.builder()
                .name(createPromoRequest.getName())
                .discount(createPromoRequest.getDiscount())
                .build();
        return promoRepository.save(promoCode);
    }

    public PromoCode updatePromoCode(PromoRequest promoRequest) {
        Optional<PromoCode> search = promoRepository.findById(promoRequest.getId());
        if (search.isEmpty()) {
            throw new PromoIDDoesntExistExeption(promoRequest.getId());
        }
        PromoCode promoCode = search.get();
        promoCode.setName(promoRequest.getName());
        promoCode.setDiscount(promoRequest.getDiscount());

        return promoRepository.save(promoCode);
    }

    public PromoCode findPromoByName(String promoName) {
        for (PromoCode promo: promoRepository.findAll()){
            if (promo.getName().equals(promoName)) {
                return promo;
            }
        }
        return null;
    }

    public PromoResponse findPromoById(Integer Id) {
        Optional<PromoCode> search = promoRepository.findById(Id);
        if (search.isEmpty()){
            throw new PromoIDDoesntExistExeption(Id);
        }
        return PromoResponse.fromPromo(search.get());
    }

    public void deleteById(Integer Id) {
        if (promoRepository.findById(Id).isEmpty()) {
            throw new PromoIDDoesntExistExeption(Id);
        }
        promoRepository.deleteById(Id);
    }
    public void deleteAll() {
        promoRepository.deleteAll();
    }

    }
