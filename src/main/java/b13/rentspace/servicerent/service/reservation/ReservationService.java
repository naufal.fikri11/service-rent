package b13.rentspace.servicerent.service.reservation;

import b13.rentspace.servicerent.dto.CreateReservationRequest;
import b13.rentspace.servicerent.dto.ReservationRequest;
import b13.rentspace.servicerent.dto.ReservationResponse;
import b13.rentspace.servicerent.model.reservation.Reservation;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface ReservationService {
    Reservation createReservation(CreateReservationRequest createReservationRequest);
    Reservation updateReservation(ReservationRequest reservationRequest);
    Reservation validateReservation(Integer id);
    List<ReservationResponse> getCustomerHistory(Integer customerID);
    List<ReservationResponse> getProviderHistory(Integer providerID);
    List<ReservationResponse> getAllReservation();
    void deleteAll();

    void deleteById(Integer id);
}
