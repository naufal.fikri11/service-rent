package b13.rentspace.servicerent.service.reservation;

import b13.rentspace.servicerent.dto.CreateReservationRequest;
import b13.rentspace.servicerent.dto.ReservationRequest;
import b13.rentspace.servicerent.dto.ReservationResponse;
import b13.rentspace.servicerent.exceptions.CustomerEmptyReservationException;
import b13.rentspace.servicerent.exceptions.ReservationNotFoundException;
import b13.rentspace.servicerent.model.reservation.Reservation;
import b13.rentspace.servicerent.repository.ReservationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReservationServiceImpl implements ReservationService{
    private final ReservationRepository reservationRepository;

    public Reservation createReservation(CreateReservationRequest createReservationRequest) {
        Integer customerID = createReservationRequest.getCustomerID();
        Integer spaceID = createReservationRequest.getSpaceID();
        Reservation reservation = Reservation.builder()
                .customerID(customerID)
                .spaceID(spaceID)
                .dateStart(createReservationRequest.getDateStart())
                .dateEnd(createReservationRequest.getDateEnd())
                .isValid(false)
                .build();
        Reservation res = reservationRepository.save(reservation);
        if(reservationRepository.customerReservationHistory.containsKey(customerID)){
            reservationRepository.customerReservationHistory.get(customerID).add(res);
        }
        else{
            List<Reservation> list = new ArrayList<>();
            list.add(res);
            reservationRepository.customerReservationHistory.put(customerID, list);
        }

        if(reservationRepository.providerReservationHistory.containsKey(spaceID)){
            reservationRepository.providerReservationHistory.get(spaceID).add(res);
        }
        else{
            List<Reservation> list = new ArrayList<>();
            list.add(res);
            reservationRepository.providerReservationHistory.put(spaceID, list);
        }
        return res;
    }

    public Reservation updateReservation(ReservationRequest reservationRequest) {
        Optional<Reservation> search = reservationRepository.findById(reservationRequest.getId());
        if (search.isEmpty()) {
            throw new ReservationNotFoundException(reservationRequest.getId());
        }
        Reservation reservation = search.get();
        reservation.setCustomerID(reservationRequest.getCustomerID());
        reservation.setSpaceID(reservationRequest.getSpaceID());
        reservation.setDateStart(reservationRequest.getDateStart());
        reservation.setDateEnd(reservationRequest.getDateEnd());
        reservation.setIsValid(reservationRequest.getIsValid());

        return reservationRepository.save(reservation);
    }

    public Reservation validateReservation(Integer id) {
        Optional<Reservation> search = reservationRepository.findById(id);
        if (search.isEmpty()) {
            throw new ReservationNotFoundException(id);
        }
        Reservation reservation = search.get();
        reservation.setIsValid(true);
        return reservationRepository.save(reservation);
    }

    public List<ReservationResponse> getCustomerHistory(Integer customerID) {
        if (!reservationRepository.customerReservationHistory.containsKey(customerID)) {
            throw new CustomerEmptyReservationException(customerID);
        }

        return reservationRepository.customerReservationHistory.get(customerID)
                .stream()
                .map(ReservationResponse::fromReservation)
                .toList();
    }

    public List<ReservationResponse> getProviderHistory(Integer providerID) {
        if (!reservationRepository.providerReservationHistory.containsKey(providerID)) {
            throw new CustomerEmptyReservationException(providerID);
        }
        return reservationRepository.providerReservationHistory.get(providerID)
                .stream()
                .map(ReservationResponse::fromReservation)
                .toList();
    }

    public List<ReservationResponse> getAllReservation() {
        return reservationRepository.findAll()
                .stream()
                .map(ReservationResponse::fromReservation)
                .toList();
    }

    public void deleteById(Integer id) {
        if (reservationRepository.findById(id).isEmpty()) {
            throw new ReservationNotFoundException(id);
        }
        reservationRepository.deleteById(id);
    }
    public void deleteAll() {
        reservationRepository.deleteAll();
    }
}
