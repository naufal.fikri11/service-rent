package b13.rentspace.servicerent.repository;

import b13.rentspace.servicerent.model.promo_code.PromoCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PromoCodeRepository extends JpaRepository<PromoCode, String> {
    @NonNull
    List<PromoCode> findAll();
    @NonNull
    Optional<PromoCode> findById(@NonNull Integer Id);
    void deleteById(@NonNull Integer Id);
}
