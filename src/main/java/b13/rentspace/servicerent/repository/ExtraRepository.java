package b13.rentspace.servicerent.repository;

import b13.rentspace.servicerent.model.extra_service.Extra;
import b13.rentspace.servicerent.model.promo_code.PromoCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExtraRepository extends JpaRepository<Extra, Integer> {
    @NonNull
    List<Extra> findAll();
    @NonNull
    Optional<Extra> findById(@NonNull Integer Id);
}

