package b13.rentspace.servicerent.repository;

import b13.rentspace.servicerent.model.promo_code.PromoCode;
import b13.rentspace.servicerent.model.reservation.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
    Map<Integer, List<Reservation>> customerReservationHistory = new HashMap<>();
    Map<Integer, List<Reservation>> providerReservationHistory = new HashMap<>();
    @NonNull
    List<Reservation> findAll();
    @NonNull
    Optional<Reservation> findById(@NonNull Integer Id);
    void deleteById(@NonNull Integer Id);
}
