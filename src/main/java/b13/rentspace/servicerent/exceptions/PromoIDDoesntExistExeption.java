package b13.rentspace.servicerent.exceptions;

public class PromoIDDoesntExistExeption extends RuntimeException{
    public PromoIDDoesntExistExeption(Integer id) {
        super("Promo with id " + id + " does not exist");
    }
}
