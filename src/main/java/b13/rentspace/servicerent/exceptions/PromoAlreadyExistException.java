package b13.rentspace.servicerent.exceptions;

public class PromoAlreadyExistException extends RuntimeException{
    public PromoAlreadyExistException(String code) {
        super("Promo with code " + code + " already exist");
    }
}
