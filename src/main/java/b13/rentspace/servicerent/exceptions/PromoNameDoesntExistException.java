package b13.rentspace.servicerent.exceptions;

public class PromoNameDoesntExistException extends RuntimeException{
    public PromoNameDoesntExistException(String name) {
        super("Promo with name " + name + " does not exist");
    }
}
