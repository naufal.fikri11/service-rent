package b13.rentspace.servicerent.exceptions;

public class CustomerEmptyReservationException extends RuntimeException{
    public CustomerEmptyReservationException(Integer id) {
        super("Customer with id " + id + " does not have any reservation");
    }
}
