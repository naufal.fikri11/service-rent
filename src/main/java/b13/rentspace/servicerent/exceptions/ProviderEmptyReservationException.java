package b13.rentspace.servicerent.exceptions;

public class ProviderEmptyReservationException extends RuntimeException{
    public ProviderEmptyReservationException(Integer id) {
        super("Provider with id " + id + " does not have any reservation");
    }
}
