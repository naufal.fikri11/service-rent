package b13.rentspace.servicerent.exceptions;

public class ReservationNotFoundException extends RuntimeException{
    public ReservationNotFoundException(Integer id) {
        super("Reservation with id " + id + " does not exist");
    }
}
