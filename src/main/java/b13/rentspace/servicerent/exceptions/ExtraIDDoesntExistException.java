package b13.rentspace.servicerent.exceptions;

public class ExtraIDDoesntExistException extends RuntimeException{
    public ExtraIDDoesntExistException(Integer id) {
        super("Extra with id " + id + " does not exist");
    }
}
