package b13.rentspace.servicerent.model.reservation;

import b13.rentspace.servicerent.model.promo_code.PromoCode;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Reservation {
    @Id
    @GeneratedValue
    private Integer id;
    private Integer customerID;
    private Integer spaceID;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private Boolean isValid;
}