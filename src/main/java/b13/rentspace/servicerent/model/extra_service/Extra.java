package b13.rentspace.servicerent.model.extra_service;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Extra {
    @Id @GeneratedValue
    private Integer id;
    private Integer reservationID;
    private String name;
}
