package b13.rentspace.servicerent.controllerTest;

import b13.rentspace.servicerent.controller.ExtraController;
import b13.rentspace.servicerent.dto.ExtraRequest;
import b13.rentspace.servicerent.dto.ExtraResponse;
import b13.rentspace.servicerent.model.extra_service.Extra;
import b13.rentspace.servicerent.service.extra.ExtraServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@WebMvcTest(controllers = ExtraController.class)
@AutoConfigureMockMvc
class ExtraControllerTest {
    private static final String END_POINT_PATH = "/api/v1/rent/extra";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExtraServiceImpl extraService;

    ExtraRequest extraRequest;
    Extra extra;
    ExtraResponse extraResponse;

    @BeforeEach
    void setUp(){
        extraRequest = ExtraRequest.builder()
                .reservationID(2)
                .name("Room Service")
                .build();

        extra = Extra.builder()
                .id(1)
                .reservationID(2)
                .name("Room Service")
                .build();

        extraResponse = ExtraResponse.builder()
                .id(1)
                .reservationID(2)
                .name("Room Service")
                .build();
    }

    @Test
    void testCreateExtraShouldReturn200OK() throws Exception {
         ObjectMapper objectMapper = new ObjectMapper();
         String requestURI = END_POINT_PATH + "/create";

         when(extraService.createExtra(any(ExtraRequest.class))).thenReturn(extra);

         mockMvc.perform(post(requestURI)
                 .contentType(MediaType.APPLICATION_JSON)
                 .content(objectMapper.writeValueAsString(extraRequest)))
                 .andExpect(status().isOk());

         verify(extraService, times(1)).createExtra(any(ExtraRequest.class));
    }

    @Test
    void testFindExtraByIdShouldReturn200OK() throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        String requestURI = END_POINT_PATH + "/findid/1";

        when(extraService.findExtraById(anyInt())).thenReturn(extraResponse);

        mockMvc.perform(get(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(extraRequest)))
                .andExpect(status().isOk());

        verify(extraService, times(1)).findExtraById(anyInt());
    }

    @Test
    void testDeleteAllShouldReturn200OK() throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        String requestURI = END_POINT_PATH + "/deleteall";

        mockMvc.perform(delete(requestURI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(extraService, times(1)).deleteAll();
    }

    @Test
    void testDeleteByIdShouldReturn200OK() throws Exception{
        String requestURI = END_POINT_PATH + "/delete/1";

        mockMvc.perform(delete(requestURI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(extraService, times(1)).deleteById(anyInt());
    }

}
