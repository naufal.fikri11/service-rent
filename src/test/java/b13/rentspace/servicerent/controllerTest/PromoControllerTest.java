package b13.rentspace.servicerent.controllerTest;
import b13.rentspace.servicerent.controller.PromoController;
import b13.rentspace.servicerent.dto.*;
import b13.rentspace.servicerent.model.promo_code.PromoCode;
import b13.rentspace.servicerent.service.promo.PromoServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;


import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PromoController.class)
@AutoConfigureMockMvc
class PromoControllerTest {
    private static final String END_POINT_PATH = "/api/v1/rent/promo";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PromoServiceImpl promoService;

    CreatePromoRequest createPromoRequest;
    PromoRequest promoRequest;
    PromoResponse promoResponse;
    PromoCode promo;

    @BeforeEach
    void setUp(){
        createPromoRequest = CreatePromoRequest.builder()
                .name("diskon lebaran")
                .discount(10)
                .build();

        promo = PromoCode.builder()
                .Id(1)
                .name("diskon lebaran")
                .discount(10)
                .build();

        promoRequest = PromoRequest.builder()
                .Id(1)
                .name("diskon natal")
                .discount(15)
                .build();

    }

    @Test
    void testCreatePromoShouldReturn200OK() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String requestURI = END_POINT_PATH + "/create";

        when(promoService.createPromoCode(createPromoRequest)).thenReturn(promo);

        mockMvc.perform(post(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createPromoRequest)))
                .andExpect(status().isOk());

        verify(promoService, times(1)).createPromoCode(createPromoRequest);
    }

    @Test
    void testUpdatePromoShouldReturn200OK() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String requestURI = END_POINT_PATH + "/update";
        PromoCode updatedPromo = PromoCode.builder()
                .Id(1)
                .name("diskon natal")
                .discount(15)
                .build();

        when(promoService.updatePromoCode(promoRequest)).thenReturn(updatedPromo);

        mockMvc.perform(put(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(promoRequest)))
                .andExpect(status().isOk());

        verify(promoService, times(1)).updatePromoCode(promoRequest);
    }

    @Test
    void testFindByIdShouldReturn200OK() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String requestURI = END_POINT_PATH + "/findid/1";

        when(promoService.findPromoById(1)).thenReturn(promoResponse);

        mockMvc.perform(get(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(promoResponse)))
                .andExpect(status().isOk());

        verify(promoService, times(1)).findPromoById(1);
    }

    @Test
    void testDeleteAllShouldReturn200OK() throws Exception {
        String requestURI = END_POINT_PATH + "/deleteall";

        mockMvc.perform(delete(requestURI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(promoService, times(1)).deleteAll();
    }

    @Test
    void testDeleteByIdShouldReturn200OK() throws Exception {
        String requestURI = END_POINT_PATH + "/delete/1";

        mockMvc.perform(delete(requestURI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(promoService, times(1)).deleteById(1);
    }
}