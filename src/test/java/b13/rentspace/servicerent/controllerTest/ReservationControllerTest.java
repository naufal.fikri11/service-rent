package b13.rentspace.servicerent.controllerTest;

import b13.rentspace.servicerent.controller.ReservationController;
import b13.rentspace.servicerent.dto.CreateReservationRequest;
import b13.rentspace.servicerent.dto.ReservationRequest;
import b13.rentspace.servicerent.dto.ReservationResponse;
import b13.rentspace.servicerent.model.reservation.Reservation;
import b13.rentspace.servicerent.service.reservation.ReservationServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;

import java.time.LocalDate;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ReservationController.class)
@AutoConfigureMockMvc
class ReservationControllerTest {
    private static final String END_POINT_PATH = "/api/v1/rent/reservation";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReservationServiceImpl reservationService;

    CreateReservationRequest createReservationRequest;
    ReservationRequest reservationRequest;
    ReservationResponse reservationResponse;
    Reservation reservation;
    @BeforeEach
    void setUp() {
        createReservationRequest = CreateReservationRequest.builder()
                .customerID(1)
                .spaceID(2)
                .dateStart(LocalDate.parse("2007-12-07"))
                .dateEnd(LocalDate.parse("2007-12-09"))
                .build();

        reservationRequest = ReservationRequest.builder()
                .Id(1)
                .customerID(1)
                .spaceID(2)
                .dateStart(LocalDate.parse("2007-12-07"))
                .dateEnd(LocalDate.parse("2007-12-09"))
                .isValid(false)
                .build();

        reservationResponse = ReservationResponse.builder()
                .id(1)
                .customerID(1)
                .spaceID(2)
                .dateStart(LocalDate.parse("2007-12-07"))
                .dateEnd(LocalDate.parse("2007-12-09"))
                .isValid(false)
                .build();

    }

    @Test
    void testCreateReservationShouldReturn200OK() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        String requestURI = END_POINT_PATH + "/create";

        when(reservationService.createReservation(createReservationRequest)).thenReturn(reservation);
        mockMvc.perform(post(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createReservationRequest)))
                .andExpect(status().isOk());

        verify(reservationService, times(1)).createReservation(createReservationRequest);
    }

    @Test
    void testUpdateReservationShouldReturn200OK() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        String requestURI = END_POINT_PATH + "/update";

        when(reservationService.updateReservation(reservationRequest)).thenReturn(reservation);

        mockMvc.perform(put(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reservationRequest)))
                .andExpect(status().isOk());

        verify(reservationService, times(1)).updateReservation(reservationRequest);
    }

    @Test
    void testValidateReservationShouldReturn200OK() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        String requestURI = END_POINT_PATH + "/validate/1";

        when(reservationService.validateReservation(1)).thenReturn(reservation);

        mockMvc.perform(put(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reservationRequest)))
                .andExpect(status().isOk());

        verify(reservationService, times(1)).validateReservation(1);
    }

    @Test
    void testGetCustomerHistoryShouldReturn200OK() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        String requestURI = END_POINT_PATH + "/customer/1";

        when(reservationService.getCustomerHistory(1)).thenReturn(java.util.List.of(reservationResponse));

        mockMvc.perform(get(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reservationRequest)))
                .andExpect(status().isOk());

        verify(reservationService, times(1)).getCustomerHistory(1);
    }

    @Test
    void testGetProviderHistoryShouldReturn200OK() throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        String requestURI = END_POINT_PATH + "/provider/1";

        when(reservationService.getProviderHistory(1)).thenReturn(java.util.List.of(reservationResponse));

        mockMvc.perform(get(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reservationRequest)))
                .andExpect(status().isOk());

        verify(reservationService, times(1)).getProviderHistory(1);
    }

    @Test
    void testGetAllReservationShouldReturn200OK() throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        String requestURI = END_POINT_PATH + "/findAll";

        when(reservationService.getAllReservation()).thenReturn(java.util.List.of(reservationResponse));

        mockMvc.perform(get(requestURI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reservationRequest)))
                .andExpect(status().isOk());

        verify(reservationService, times(1)).getAllReservation();
    }

    @Test
    void testDeleteByIdShouldReturn200OK() throws Exception{
        String requestURI = END_POINT_PATH + "/delete/1";

        mockMvc.perform(delete(requestURI)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(reservationService, times(1)).deleteById(1);
    }
}
