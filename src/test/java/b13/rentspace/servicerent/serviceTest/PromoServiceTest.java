package b13.rentspace.servicerent.serviceTest;

import b13.rentspace.servicerent.dto.CreatePromoRequest;
import b13.rentspace.servicerent.dto.PromoRequest;
import b13.rentspace.servicerent.dto.PromoResponse;
import b13.rentspace.servicerent.exceptions.PromoAlreadyExistException;
import b13.rentspace.servicerent.exceptions.PromoIDDoesntExistExeption;
import b13.rentspace.servicerent.model.promo_code.PromoCode;
import b13.rentspace.servicerent.repository.PromoCodeRepository;
import b13.rentspace.servicerent.service.promo.PromoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PromoServiceTest {

    @Mock
    private PromoCodeRepository promoRepository;
    @InjectMocks
    private PromoServiceImpl promoService;

    CreatePromoRequest createPromoRequest;
    PromoCode promoCode;
    PromoRequest promoRequest;
    PromoResponse promoResponse;

    @BeforeEach
    void setUp() {
        createPromoRequest = CreatePromoRequest.builder()
                .name("Lebaran")
                .discount(10)
                .build();
        promoCode = PromoCode.builder()
                .Id(1)
                .name("Lebaran")
                .discount(10)
                .build();
        promoRequest = PromoRequest.builder()
                .Id(1)
                .name("Tahun Baru")
                .discount(15)
                .build();
        promoResponse = PromoResponse.builder()
                .Id(1)
                .name("Lebaran")
                .discount(10)
                .build();
    }

    @Test
    void whenCreatePromoShouldReturnPromo() {
        // Arrange
        when(promoRepository.save(any(PromoCode.class))).thenReturn(promoCode);
        when(promoRepository.findAll()).thenReturn(java.util.List.of());

        // Act
        PromoCode result = promoService.createPromoCode(createPromoRequest);

        // Assert
        assertNotNull(result);
        assertEquals(promoCode, result);
        verify(promoRepository, times(1)).save(any(PromoCode.class));
    }

    @Test
    void whenCreatePromoWithSameNameShouldThrowError() {
        // Arrange
        when(promoRepository.findAll()).thenReturn(java.util.List.of(promoCode));

        // Act & Assert
        assertThrows(PromoAlreadyExistException.class, () -> promoService.createPromoCode(createPromoRequest));
        verify(promoRepository, times(0)).save(any(PromoCode.class));
    }

    @Test
    void whenUpdatePromoReturnPromoCode() {

        PromoCode updated = PromoCode.builder()
                .Id(1)
                .name("Tahun Baru")
                .discount(15)
                .build();

        when(promoRepository.findById(1)).thenReturn(Optional.of(promoCode));
        when(promoRepository.save(any(PromoCode.class))).thenReturn(updated);

        PromoCode result = promoService.updatePromoCode(promoRequest);

        assertEquals(updated, result);
        verify(promoRepository, times(1)).findById(1);
        verify(promoRepository, times(1)).save(any(PromoCode.class));
    }

    @Test
    void whenUpdatePromoWithNonExistingIdShouldThrowError() {
        // Arrange
        when(promoRepository.findById(1)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(PromoIDDoesntExistExeption.class, () -> promoService.updatePromoCode(promoRequest));
        verify(promoRepository, times(0)).save(any(PromoCode.class));
    }

    @Test
    void whenFindPromoByNameShouldReturnPromoCode() {
        // Arrange
        when(promoRepository.findAll()).thenReturn(java.util.List.of(promoCode));

        // Act
        PromoCode result = promoService.findPromoByName("Lebaran");

        // Assert
        assertNotNull(result);
        assertEquals(promoCode, result);
        verify(promoRepository, times(1)).findAll();
    }

    @Test
    void whenFindPromoByNameNotFoundShouldReturnNull() {
        PromoCode result = promoService.findPromoByName("Tahun Baru");

        // Assert
        assertNull(result);
        verify(promoRepository, times(1)).findAll();
    }

    @Test
    void whenFindPromoByIdShouldReturnPromoResponse() {
        when(promoRepository.findById(1)).thenReturn(Optional.of(promoCode));
        PromoResponse result = promoService.findPromoById(1);

        assertEquals(promoResponse, result);
        verify(promoRepository, times(1)).findById(1);
    }

    @Test
    void whenFindPromoByIdNotFoundShouldThrowException(){
        assertThrows(PromoIDDoesntExistExeption.class, () -> promoService.findPromoById(1));
    }

    @Test
    void whenDeleteByIdShouldDeletePromoCode() {
        // Arrange
        when(promoRepository.findById(1)).thenReturn(Optional.of(promoCode));

        // Act
        promoService.deleteById(1);

        // Assert
        verify(promoRepository, times(1)).deleteById(1);
    }

    @Test
    void whenDeleteByIdNotFoundShouldThrowException() {
        // Arrange
        when(promoRepository.findById(1)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(PromoIDDoesntExistExeption.class, () -> promoService.deleteById(1));
        verify(promoRepository, times(0)).deleteById(1);
    }

    @Test
    void whenDeleteAllShouldDeleteAll(){
        promoService.deleteAll();
        verify(promoRepository, times(1)).deleteAll();
    }
}

