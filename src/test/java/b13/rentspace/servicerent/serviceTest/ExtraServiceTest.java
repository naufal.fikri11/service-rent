package b13.rentspace.servicerent.serviceTest;

import b13.rentspace.servicerent.dto.*;
import b13.rentspace.servicerent.exceptions.ExtraIDDoesntExistException;
import b13.rentspace.servicerent.exceptions.PromoAlreadyExistException;
import b13.rentspace.servicerent.exceptions.PromoIDDoesntExistExeption;
import b13.rentspace.servicerent.model.extra_service.Extra;
import b13.rentspace.servicerent.model.promo_code.PromoCode;
import b13.rentspace.servicerent.repository.ExtraRepository;
import b13.rentspace.servicerent.repository.PromoCodeRepository;
import b13.rentspace.servicerent.service.extra.ExtraServiceImpl;
import b13.rentspace.servicerent.service.promo.PromoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExtraServiceTest {

    @Mock
    private ExtraRepository extraRepository;
    @InjectMocks
    private ExtraServiceImpl extraService;

    ExtraRequest extraRequest;
    Extra extra;
    ExtraResponse extraResponse;
    @BeforeEach
    void setUp() {
        extraRequest = ExtraRequest.builder()
                .reservationID(1)
                .name("Kamar Mandi")
                .build();
        extra = Extra.builder()
                .id(1)
                .reservationID(1)
                .name("Kamar Mandi")
                .build();
        extraResponse = ExtraResponse.builder()
                .id(1)
                .reservationID(1)
                .name("Kamar Mandi")
                .build();
    }

    @Test
    void whenCreateExtraShouldReturnExtra() {
        // Arrange
       when(extraRepository.save(any(Extra.class))).thenReturn(extra);

        // Act
        Extra result = extraService.createExtra(extraRequest);

        // Assert
        assertEquals(extra, result);
        verify(extraRepository, times(1)).save(any(Extra.class));
    }

    @Test
    void whenFindExtraByIdShouldReturnExtraResponse() {
        // Arrange
        when(extraRepository.findById(anyInt())).thenReturn(Optional.of(extra));

        // Act
        ExtraResponse result = extraService.findExtraById(1);

        // Assert
        assertEquals(extraResponse, result);
        verify(extraRepository, times(1)).findById(anyInt());
    }

    @Test
    void whenFindExtraByIdDoesntExistShouldThrowException(){
        when(extraRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(ExtraIDDoesntExistException.class, () -> extraService.findExtraById(1));
    }

    @Test
    void whenDeleteByIdShouldDeleteExtra() {
        // Arrange
        when(extraRepository.findById(anyInt())).thenReturn(Optional.of(extra));

        // Act
        extraService.deleteById(1);

        // Assert
        verify(extraRepository, times(1)).deleteById(anyInt());
    }

    @Test
    void whenDeleteByIdDoesntExistShouldThrowException(){
        when(extraRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(ExtraIDDoesntExistException.class, () -> extraService.deleteById(1));
    }

    @Test
    void whenDeleteAllShouldInvokeDeleteAll() {
        // Act
        extraService.deleteAll();

        // Assert
        verify(extraRepository).deleteAll();
    }
}

