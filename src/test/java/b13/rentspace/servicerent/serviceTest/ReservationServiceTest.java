package b13.rentspace.servicerent.serviceTest;

import b13.rentspace.servicerent.dto.CreateReservationRequest;
import b13.rentspace.servicerent.dto.ReservationRequest;
import b13.rentspace.servicerent.dto.ReservationResponse;
import b13.rentspace.servicerent.exceptions.CustomerEmptyReservationException;
import b13.rentspace.servicerent.model.reservation.Reservation;
import b13.rentspace.servicerent.repository.ReservationRepository;
import b13.rentspace.servicerent.service.reservation.ReservationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReservationServiceTest {
    @InjectMocks
    private ReservationServiceImpl reservationService;

    @Mock
    private ReservationRepository reservationRepository;

    Reservation reservation;
    CreateReservationRequest createReservationRequest;
    ReservationRequest reservationRequest;
    Reservation updated;
    ReservationResponse reservationResponse;

    @BeforeEach
    void setup(){
        createReservationRequest = CreateReservationRequest.builder()
                .customerID(2)
                .spaceID(3)
                .dateStart(LocalDate.parse("2007-12-03"))
                .dateEnd(LocalDate.parse("2007-12-05"))
                .build();

        reservationRequest = ReservationRequest.builder()
                .Id(1)
                .customerID(4)
                .spaceID(5)
                .dateStart(LocalDate.parse("2007-12-07"))
                .dateEnd(LocalDate.parse("2007-12-09"))
                .isValid(false)
                .build();

        reservation = Reservation.builder()
                .id(1)
                .customerID(2)
                .spaceID(3)
                .dateStart(LocalDate.parse("2007-12-03"))
                .dateEnd(LocalDate.parse("2007-12-05"))
                .isValid(false)
                .build();

        updated = Reservation.builder()
                .id(1)
                .customerID(4)
                .spaceID(5)
                .dateStart(LocalDate.parse("2007-12-07"))
                .dateEnd(LocalDate.parse("2007-12-09"))
                .isValid(false)
                .build();
    }

    @Test
    void whenCreateReservationShouldReturnReservation(){
        when(reservationRepository.save(any(Reservation.class))).thenReturn(reservation);
        Reservation result = reservationService.createReservation(createReservationRequest);

        verify(reservationRepository, times(1)).save(any(Reservation.class));
        assertEquals(reservation, result);
        assertEquals(1, reservationRepository.customerReservationHistory.size());
        assertEquals(1, reservationRepository.providerReservationHistory.size());
    }

    @Test
    void whenUpdateReservationShouldReturnReservation(){
        when(reservationRepository.findById(anyInt())).thenReturn(java.util.Optional.ofNullable(reservation));
        when(reservationRepository.save(any(Reservation.class))).thenReturn(reservation);
        Reservation result = reservationService.updateReservation(reservationRequest);

        verify(reservationRepository, atLeastOnce()).findById(anyInt());
        verify(reservationRepository, times(1)).save(any(Reservation.class));
        assertEquals(updated, result);
    }

    @Test
    void whenUpdateReservationDoesntExistShouldThrowExeception(){
        when(reservationRepository.findById(anyInt())).thenReturn(java.util.Optional.empty());
        assertThrows(RuntimeException.class, () -> reservationService.updateReservation(reservationRequest));
    }

    @Test
    void whenValidatingReservationShouldChangeIsValidToTrue(){
        when(reservationRepository.findById(anyInt())).thenReturn(java.util.Optional.ofNullable(reservation));
        when(reservationRepository.save(any(Reservation.class))).thenReturn(reservation);
        Reservation result = reservationService.validateReservation(1);

        verify(reservationRepository, atLeastOnce()).findById(anyInt());
        verify(reservationRepository, times(1)).save(any(Reservation.class));
        assertEquals(true, result.getIsValid());
    }

    @Test
    void whenValidatingReservationDoesntExistShouldThrowExeception(){
        when(reservationRepository.findById(anyInt())).thenReturn(java.util.Optional.empty());
        assertThrows(RuntimeException.class, () -> reservationService.validateReservation(1));
    }

    @Test
    void whenGetCustomerHistoryShouldReturnListReservationResponse(){
        when(reservationRepository.save(any(Reservation.class))).thenReturn(reservation);
        Reservation result = reservationService.createReservation(createReservationRequest);
        ReservationResponse result1 = ReservationResponse.fromReservation(result);
        ReservationResponse result2 = reservationService.getCustomerHistory(2).get(0);

        assertEquals(reservationRepository.customerReservationHistory.size(), 1);
        assertEquals(result1, result2);
    }


    @Test
    void whenGetCustomerHistoryDoesntExistShouldThrowException(){
        assertThrows(CustomerEmptyReservationException.class, () -> {
            reservationService.getCustomerHistory(99);
        });
    }

    @Test
    void whenGetProviderHistoryShouldReturnListReservationResponse(){
        when(reservationRepository.save(any(Reservation.class))).thenReturn(reservation);
        Reservation result = reservationService.createReservation(createReservationRequest);
        ReservationResponse result1 = ReservationResponse.fromReservation(result);
        ReservationResponse result2 = reservationService.getProviderHistory(3).get(0);

        assertEquals(reservationRepository.providerReservationHistory.size(), 1);
        assertEquals(result1, result2);
    }

    @Test
    void whenGetProviderHistoryDoesntExistShouldThrowException(){
        assertThrows(CustomerEmptyReservationException.class, () -> {
            reservationService.getProviderHistory(99);
        });
    }

    @Test
    void whenGetAllReservationShouldReturnAllReservation(){
        when(reservationRepository.save(any(Reservation.class))).thenReturn(reservation);
        when(reservationRepository.findAll()).thenReturn(List.of(reservation));
        Reservation result = reservationService.createReservation(createReservationRequest);
        ReservationResponse result1 = ReservationResponse.fromReservation(result);
        ReservationResponse result2 = reservationService.getAllReservation().get(0);

        verify(reservationRepository, atLeastOnce()).findAll();
        assertEquals(reservationService.getAllReservation().size(), 1);
        assertEquals(result1, result2);
    }

    @Test
    void whenDeleteByIdShouldDeleteById(){
        // Arrange
        when(reservationRepository.findById(1)).thenReturn(Optional.of(reservation));

        // Act
        reservationService.deleteById(1);

        // Assert
        verify(reservationRepository, times(1)).deleteById(1);
    }

    @Test
    void whenDeleteByIdShouldThrowException(){
        // Arrange
        when(reservationRepository.findById(1)).thenReturn(Optional.empty());

        // Act
        assertThrows(RuntimeException.class, () -> reservationService.deleteById(1));
    }

    @Test
    void whenDeleteAllShouldDeleteAll(){
        reservationService.deleteAll();
        verify(reservationRepository, times(1)).deleteAll();
    }
}
