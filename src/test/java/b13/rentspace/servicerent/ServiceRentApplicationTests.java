package b13.rentspace.servicerent;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ServiceRentApplicationTests.class)
class ServiceRentApplicationTests {

	@Test
	void contextLoads() {
	}

}
